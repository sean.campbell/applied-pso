function [Iout] = regionGrouping(Iout, Iin, thresholds, swarmSettings)
numSegments = length(thresholds) + 1;

nIout = Iout;

for area = swarmSettings.minArea : swarmSettings.minAreaIncrements : ...
          swarmSettings.maxMinArea

    % Build up a highly segmented image and LUT for region statistics
    [vSegIm, lut, mergeableRegions] = build(nIout, Iin, numSegments, ...
                                                area);
    % For every merge candidate
    for a = 1 : length(mergeableRegions)    
        % Fetch region info
        region = mergeableRegions(a);
        stats = lut{region};

        % Obtain immediate non-mergeable neighbours
        msk = (vSegIm == region);
        neighbours = unique(vSegIm(imdilate(msk, true(3)) & ~msk));
        candNeighbours = neighbours(~ismember(neighbours, mergeableRegions));

        % If no immediate neighbours are non-mergeable, try neighbours of
        % neighbours
        if(isempty(candNeighbours))
            candNeighbours = neighbours;
            for b = 1 : length(neighbours)
               msk = (vSegIm == neighbours(b));
               candNeighbours = [candNeighbours; ...
                        unique(vSegIm(imdilate(msk, true(3)) & ~msk))];
            end
            candNeighbours = candNeighbours(~ismember(candNeighbours, ...
                                mergeableRegions));
        end

        % If neighbours still empty, give up on merging that region;
        % could build a recursive function but would likely become very
        % inaccurate
        if(~isempty(candNeighbours))
            % Find the closest neighbour in terms of mean intensity
            closestNeighbour = candNeighbours(1);
            closestMean = lut{candNeighbours(1)}.MeanIntensity;
            for b = 2 : length(candNeighbours)        
                if(interp1([closestMean+1e-9 lut{candNeighbours(b)}.MeanIntensity], ...
                            1:2, stats.MeanIntensity, 'nearest') == 2)
                    closestNeighbour = candNeighbours(b);
                    closestMean = lut{candNeighbours(b)}.MeanIntensity;
                end
            end

            nIout(msk) = lut{closestNeighbour}.InitialRegion;
        else
            disp('Unable to merge a region');
        end
    end

    % Show the difference
    % showColourSegments(nIout);
end
Iout = nIout;
end

function [vSegIm, lut, mergeableRegions] = build(Iout, Iin, ...
                                                    numSegments, minArea)
vSegIm = zeros(size(Iout));
lut = {};
mergeableRegions = [];
idx = 1;
for a = 1 : numSegments
    msk = (Iout == a);
    stats = regionprops(msk, Iin, 'Area', 'PixelIdxList', ...
        'MeanIntensity', 'PixelValues');
    [stats.InitialRegion] = deal(a);
    llut = cell(1, length(stats));
    for b = 1 : length(stats)
        vSegIm(stats(b).PixelIdxList) = idx;
        llut{b} = stats(b);        
        if(stats(b).Area < minArea)
            mergeableRegions = [mergeableRegions; idx];
        end
        idx = idx + 1;
    end
    lut = horzcat(lut, llut); % Better than growing by one every iteration
end
end