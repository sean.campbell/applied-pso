% Calculate values:
% SI    = area of full image
% Rj    = set of pixels in region j
% Sj    = |Rj| denotes the area of region j
% Cx(p) = value of grayscale for pixel p
% Cx(Rj) = Sum of pixel values in region j divided by area of region

function [fitness] = evaluationFunctionLiuYang(im, tMat)
% Cache variables
numParticles = size(tMat, 1);
numThresh = size(tMat, 2);
mult = sqrt(numThresh+1);

% Initialise the fitness array
fitness = zeros(numParticles, 1);

% Check for parallel license
if(license('test', 'Distrib_Computing_Toolbox'))
    if(isempty(gcp('nocreate')))
        parpool('local', feature('numCores'));
    end
end

% Perform in parallel for each particle
parfor a = 1 : numParticles
    % Segment the image
    segmentedImage = segmentImage(im, tMat(a, :));
    
    colourError = zeros(1, numThresh + 1);
    
    % For each region in the segmented image
    for b = 1 : numThresh + 1
        msk = (segmentedImage == b);
        sj = sum(msk(:)); % Area of region
        
        rj = im(msk); % Set of pixels in region j
        cxrj = sum(rj(:))/sj;
        colourError(b) = sum((rj - cxrj).^2) / sqrt(sj);
    end
    fitness(a) = mult*sum(colourError(:));
end
end

% idx = find(segmentedImages(:, :, a) == b); % Numbering assumption
% sj = length(idx); % Area of region j
% 
% rj = im(idx); % Set of pixels in region j
% cxrj = sum(rj(:))/sj; % Average of region
% colourError(b) = sum((rj - cxrj).^2) / sqrt(sj);
