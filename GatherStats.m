%% Typical startup to script
clearvars();
close all;
clc;

%% Quick script for generating statistics for each algorithm
%% Input stage
% A, E, C, G, J
name = 'testImageA.jpg';
im = imread(name);
figure, imshow(im);
%% Quick display
g = zeros(size(im, 1), size(im, 2), 1, 4);
g(:, :, 1, 1) = showGraySegments(segmentImage(im, [99, 140]));
g(:, :, 1, 2) = showGraySegments(segmentImage(im, [96, 151, 216]));
g(:, :, 1, 3) = showGraySegments(segmentImage(im, [92, 144, 170, 230]));
g(:, :, 1, 4) = showGraySegments(segmentImage(im, [89, 135, 161, 187, 225]));
imdisp(g, 'Size', [1 4]);
%% Initialisation
% Segmentation control
numSegments = 6;
fitnessFunction = @evaluationFunctionBorsotti;
maxOrMinFn = @min;
method = 2; % DPSO, PSO, SA, SS

% Fetch the swarm-settings for further info
swarmSettings = psoSegmentation();
numIters = swarmSettings.size*swarmSettings.maxIterations;
minVal = swarmSettings.imMin;
maxVal = swarmSettings.imMax;

% Number of times to perform the segmentation for stats gathering
numTimesToSegment = 20;

% Select the algortihm
if method == 1
    segFunc = @() dpsoSegmentation(im, numSegments, ...
                    swarmSettings);
elseif method == 2
    swarmSettings.evaluationFunction = fitnessFunction;
    swarmSettings.maxOrMin = maxOrMinFn;
    
    segFunc = @() psoSegmentation(im, numSegments, ...
                    swarmSettings);
elseif method == 3
    saSettings = simulatedAnnealingSegmentation();
    saSettings.maxIterations = numIters;
    saSettings.evaluationFunction = fitnessFunction;
    saSettings.maxOrMin = maxOrMinFn;
    segFunc = @() simulatedAnnealingSegmentation(im, numSegments, ...
                    saSettings);
elseif method == 4
   % SS segmentation function
    segFunc = @() randomSegmentation(im, numSegments, fitnessFunction, ...
                maxOrMinFn, numIters, minVal, maxVal);
else
    error('Unrecognised method');
end
            
% Matrices for storage and analysis
optimalThresholds = zeros(numTimesToSegment, numSegments - 1);
fitness = zeros(numTimesToSegment, 1);
executionTimes = zeros(numTimesToSegment, 1);

%% Warm up the CPU 
for a = 1 : 2
    % Just segment
    segFunc();
end

%% Perform the segmentations
for a = 1 : numTimesToSegment
    % Segmentation
    [~, thresholds, ftn, time] = segFunc();
    
    % Assign parameters to matrices
    optimalThresholds(a, :) = thresholds;
    fitness(a) = ftn;
    executionTimes(a) = time;
    disp(a);
end

%% Calculate the statistics
meanFitness = mean(fitness);
stdFitness = std(fitness);

meanThresholds = mean(optimalThresholds, 1);
stdThresholds = std(optimalThresholds, 1);
meanStd = mean(stdThresholds);

rMeanThresholds = round(meanThresholds);

meanExecTime = mean(executionTimes);
