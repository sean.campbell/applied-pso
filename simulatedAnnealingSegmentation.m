function [Iout, thresholds, bestFitness, time] = simulatedAnnealingSegmentation(Iin, ...
    numSegments, saSettings)
% Method to acquire the settings
if nargin == 0
    Iout = createSAControl();
    return
end

% Check if settings provided
if ~exist('swarmSettings', 'var')
    saSettings = createSAControl();
end

% Create a random solution given the constraints
currentSolution = sort(randi([saSettings.imMin saSettings.imMax], ...
                        1, numSegments - 1), 2);
currentFitness = feval(saSettings.evaluationFunction, Iin, ...
                        currentSolution);

% Hold reference to best
bestSolution = currentSolution;
bestFitness = currentFitness;

% Create initial temperature
cTemp = saSettings.maxTemp;
tic;
% Explore the search space
for a = 1 : saSettings.maxIterations
    neighbour = createNeighbour(currentSolution, saSettings);
    neighbourFitness = feval(saSettings.evaluationFunction, ...
                                Iin, neighbour);                         
    cTemp = cTemp * saSettings.coolingRate;
    
    if(a == (0.7*saSettings.maxIterations))
        saSettings.maxNeighbourDist = floor(0.5* ...
                                            saSettings.maxNeighbourDist);
        saSettings.minNeighbourDist = ceil(0.5* ...
                                            saSettings.minNeighbourDist);
    elseif(a == (0.9*saSettings.maxIterations))
        saSettings.maxNeighbourDist = floor(0.25* ...
                                            saSettings.maxNeighbourDist);
        saSettings.minNeighbourDist = ceil(0.25* ...
                                            saSettings.minNeighbourDist);
    end
    
    if(feval(saSettings.maxOrMin, neighbourFitness, currentFitness) == ...
            neighbourFitness)
        currentSolution = neighbour;
        currentFitness = neighbourFitness;
        
        if(feval(saSettings.maxOrMin, neighbourFitness, bestFitness) ...
                == neighbourFitness)
            bestSolution = currentSolution;
            bestFitness = currentFitness;
        end
    elseif(exp((currentFitness - neighbourFitness)/cTemp) > ...
                unifinv(rand()))
        currentSolution = neighbour;
        currentFitness = neighbourFitness;
    end
end
time = toc;
% Assign outputs
Iout = segmentImage(Iin, bestSolution);
thresholds = bestSolution;
end

function [saSettings] = createSAControl()
saSettings = {};

% SA Parameters
saSettings.maxIterations = 1000;
saSettings.maxTemp = 1000;
saSettings.coolingRate = 0.95;

% Fitness function and type
saSettings.evaluationFunction = @evaluationFunctionCustom;
saSettings.maxOrMin = @max;

% Segment image as if it were uint8
saSettings.imMin = 1;
saSettings.imMax = 254;
saSettings.isFloat = false;

% Neighbour info
saSettings.maxNeighbourDist = 10;
saSettings.minNeighbourDist = -10;
end

function [solution] = createNeighbour(currentSolution, saSettings)
solution = sort(currentSolution + ...
                unifinv(rand(1, length(currentSolution)), ...
                saSettings.minNeighbourDist, ...
                saSettings.maxNeighbourDist), 2);
if(~saSettings.isFloat)
    solution = round(solution);
end
            
solution = ((solution <= saSettings.imMin).*saSettings.imMin + ...
            (solution > saSettings.imMin).*solution);
solution = ((solution >= saSettings.imMax).*saSettings.imMax + ...
            (solution < saSettings.imMax).*solution);         
end
