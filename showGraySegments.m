function [grayIm] = showGraySegments(segIm)
% Assume image segmented and typical 8-bit image
segs = unique(segIm);
numSegs = length(segs);

% Basic check
assert (numSegs < 255);

% Create gray-scale image
thresholds = round(0:255/(numSegs+1):255);

% Make the image
grayIm = uint8(zeros(size(segIm)));
for a = 1 : numSegs
    msk = (segIm == segs(a));
    grayIm(msk) = thresholds(a);
end
% Display
figure, imshow(grayIm);
end