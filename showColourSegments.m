function showColourSegments(segIm)
% Assume image segmented and typical 24-bit RGB image
segs = unique(segIm);
numSegs = length(segs);
channelMax = 255;

if numSegs == 2
    partitions = [0 0 0; 255 255 255];
elseif numSegs == 3
    partitions = [255 0 0; 0 255 0; 0 0 255];
elseif numSegs == 4
    partitions = [0 0 0; 255 0 0; 0 255 0; 0 0 255];
elseif numSegs == 5
    partitions = [0 0 0; 255 0 0; 0 255 0; 0 0 255; 255 255 255];
else
    % Calculate equally spaced thresholds and permutations of colour space
    thresholds = round(0:channelMax/(numSegs+1):channelMax);
    permutations = npermutek(thresholds, 3);

    % Select random partitions
    partitions = permutations(randperm(length(permutations), numSegs), :);
end

% Make the nice RGB segmented image
rgbIm = uint8(zeros(size(segIm, 1), size(segIm, 2), 3));
for a = 1 : numSegs
    msk = (segIm == segs(a));
    for b = 1 : 3
        channel = rgbIm(:, :, b);
        channel(msk) = partitions(a, b);
        rgbIm(:, :, b) = channel;
    end    
end
% Display
figure, imshow(rgbIm);
end