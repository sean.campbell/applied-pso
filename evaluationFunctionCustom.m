function [fitness] = evaluationFunctionCustom(im, tMat)
% Calculate probability of pixel info
probPixel = imhist(im)'./numel(im);

% Initialisaton
numThresh = size(tMat, 2);
fitness = zeros(size(tMat, 1), 1);

% Calculate fitness minimising intra-class variance based upon the image
% histogram
for a = 1 : size(tMat, 1)    
    fitness(a)=sum(probPixel(1:tMat(a,1)))*(sum((1:tMat(a,1)).*probPixel(1:tMat(a,1))/sum(probPixel(1:tMat(a,1)))) - sum((1:256).*probPixel(1:256)) )^2;
    for thresh = 2 : numThresh
        fitness(a)=fitness(a)+sum(probPixel(tMat(a,thresh-1)+1:tMat(a,thresh)))*(sum((tMat(a,thresh-1)+1:tMat(a,thresh)).*probPixel(tMat(a,thresh-1)+1:tMat(a,thresh))/sum(probPixel(tMat(a,thresh-1)+1:tMat(a,thresh))))- sum((1:256).*probPixel(1:256)))^2;
    end
    fitness(a)=fitness(a)+sum(probPixel(tMat(a,numThresh)+1:256))*(sum((tMat(a,numThresh)+1:256).*probPixel(tMat(a,numThresh)+1:256)/sum(probPixel(tMat(a,numThresh)+1:256)))- sum((1:256).*probPixel(1:256)))^2;
end
end