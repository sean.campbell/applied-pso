function [fitness] = evaluationFunctionBorsotti(im, tMat)
% Cache variables
numParticles = size(tMat, 1);
numThresh = size(tMat, 2);
mult = (1/(1000*numel(im)))*sqrt(numThresh+1);

% Initialise the fitness array
fitness = zeros(numParticles, 1);

% Check for parallel license
if(license('test', 'Distrib_Computing_Toolbox'))
    if(isempty(gcp('nocreate')))
        parpool('local', feature('numCores'));
    end
end

% Perform in parallel for each particle
parfor a = 1 : numParticles
    % Segment the image
    segmentedImage = segmentImage(im, tMat(a, :));
    
    % Find the areas    
    sjs = histc(segmentedImage(:), 1:1:(numThresh+1));
        
    % Find the counts of areas with that size
    countSjs = zeros(1, numThresh + 1);
    for b = 1 : numThresh + 1
        msk = (sjs == sjs(b));
        countSjs(b) = sum(msk(:));
    end
    
    colourError = zeros(1, numThresh + 1);
    % For each region in the segmented image
    for b = 1 : numThresh + 1
        msk = (segmentedImage == b);
        
        rj = im(msk); % Set of pixels in region j
        cxrj = sum(rj(:))/sjs(b);
        colourError(b) = (sum((rj - cxrj).^2) / (1 + log(sjs(b)))) + ...
                        (countSjs(b)/sjs(b))^2;
    end
    fitness(a) = mult*sum(colourError(:));
end
end
