function [Iout, thresholds, bestFitness, psoTime, mIout] = psoSegmentation(Iin, numSegments, ...
                                                        swarmSettings)
% Method to acquire the settings
if nargin == 0
    Iout = createPSOControl();
    return
end

% Check if settings provided
if ~exist('swarmSettings', 'var')
    swarmSettings = createPSOControl();
end

% Basic checks
assert(ismatrix(Iin) && numSegments > 1);

% Check if LPF of image desired
if(swarmSettings.performLPF)
    Iin = imgaussfilt(Iin, 0.6);
    figure, imshow(Iin);
end

% Perform timing
tic;

% Initialise swarm
swarm = createSwarm(swarmSettings, Iin, numSegments - 1);

% Perform the PSO algorithm
for a = 1 : swarmSettings.maxIterations
    % Check if swarm visualisation desired
    if(swarmSettings.plotSwarm)
        plotSwarm(swarmSettings, swarm);
    end
    
    % Update velocity
    swarm = updateVelocity(swarmSettings, swarm);
    
    % Update position
    swarm = updatePosition(swarmSettings, swarm);
    
    % Repair precaution
    swarm.pos = sort(swarm.pos, 2);
    
    % Perform fitness evaluation
    swarm.fitness = feval(swarmSettings.evaluationFunction, Iin, ...
                            swarm.pos);
    %swarmSettings.evaluationFunction(Iin, swarm);
    
    % Update the swarm best stats
    swarm = updateBest(swarmSettings, swarm);
    
    % Break loop if stagnant
    if(swarm.stagnation == swarmSettings.stagnation)
        disp('Stagnation occurred');
        break;
    end
end

% Assign execution time
psoTime = toc;

% Finally perform the segmentation
Iout = segmentImage(Iin, swarm.best);
thresholds = swarm.best;
bestFitness = swarm.bestFitness;
%showGraySegments(Iout);

% Clean-up small isolated regions if desired
if(swarmSettings.mergeRegions)
    mIout = regionGrouping(Iout, Iin, thresholds, swarmSettings);
    showGraySegments(mIout);
else
    mIout = Iout;
end
end

function [swarmSettings] = createPSOControl()
swarmSettings = {};

% Size and evolution
swarmSettings.size = 40;
swarmSettings.maxIterations = 80;

% Velocity control
swarmSettings.maxVelocity = 4;
swarmSettings.minVelocity = -4;
swarmSettings.w = 1.2;  % Inertia constant
swarmSettings.c1 = 2.0; % Personal best bias / Individual weight
swarmSettings.c2 = 2.0; % Global best bias / Social weight

% Fitness function and type
swarmSettings.evaluationFunction = @evaluationFunctionCustom;
swarmSettings.maxOrMin = @max;

% Stopping criterion - Stagnation of best particle
swarmSettings.stagnation = floor(0.5*(swarmSettings.maxIterations));

% Perform LPF prior to segmentation
swarmSettings.performLPF = false;

% Segment image as if it were uint8
swarmSettings.imMin = 0;
swarmSettings.imMax = 255;
swarmSettings.isFloat = false;

% Plot swarm thresholds over time
swarmSettings.plotSwarm = false;
swarmSettings.pauseTime = 0.05;

% Region merging criterion
swarmSettings.mergeRegions = false;
swarmSettings.minArea = 10;
swarmSettings.minAreaIncrements = 10;
swarmSettings.maxMinArea = 20;
end

function [swarm] = createSwarm(swarmSettings, im, numThresh)
swarm = {};

swarm.veloc = unifinv(rand(swarmSettings.size, numThresh), ... % Random init veloc
    0.1*swarmSettings.minVelocity, 0.1*swarmSettings.maxVelocity);

swarm.pos = sort(unifinv(rand(swarmSettings.size, numThresh), ... % Random init thresh
    swarmSettings.imMin, swarmSettings.imMax), 2);
if(~swarmSettings.isFloat)
    swarm.pos = round(swarm.pos);
end

swarm.fitness = swarmSettings.evaluationFunction(im, swarm.pos);

swarm.particleBest = swarm.pos;
swarm.particleBestFitness = swarm.fitness;

[b, idx] = feval(swarmSettings.maxOrMin, swarm.fitness);
swarm.best = swarm.pos(idx, :);
swarm.bestFitness = b;

swarm.prevBest = swarm.best;
swarm.stagnation = 0;
end

function [swarm] = updateVelocity(swarmSettings, swarm)
% Cache sizes of veloc
szs = size(swarm.veloc);

% Generate random numbers
randA = rand(szs);
randB = rand(szs);

% Create global best vector
gBest = ones(szs(1), 1)*swarm.best;

% Generate new velocity
veloc = swarmSettings.w .* swarm.veloc + ... % Inertial component
    randA.*(swarmSettings.c1.*(swarm.particleBest - swarm.pos)) + ...
    randB.*(swarmSettings.c2.*(gBest - swarm.pos));

if(~swarmSettings.isFloat)
    veloc = round(veloc);
end

veloc = ((veloc <= swarmSettings.minVelocity).*swarmSettings.minVelocity + ...
    (veloc > swarmSettings.minVelocity).*veloc);

veloc = ((veloc >= swarmSettings.maxVelocity).*swarmSettings.maxVelocity + ...
    (veloc < swarmSettings.maxVelocity).*veloc);

% Update swarm velocity
swarm.veloc = veloc;
end

function [swarm] = updatePosition(swarmSettings, swarm)
pos = swarm.pos + swarm.veloc;

pos = ((pos <= swarmSettings.imMin).*swarmSettings.imMin + ...
    (pos > swarmSettings.imMin).*pos);
pos = ((pos >= swarmSettings.imMax).*swarmSettings.imMax + ...
    (pos < swarmSettings.imMax).*pos);

% Update swarm position
swarm.pos = pos;
end

function [swarm] = updateBest(swarmSettings, swarm)
bestUpdated = false;
for a = 1 : swarmSettings.size
    cftn = swarm.fitness(a);
    % Update the particle history
    if(feval(swarmSettings.maxOrMin, cftn, swarm.particleBestFitness(a)) ...
            == cftn)
        swarm.particleBest(a, :) = swarm.pos(a, :);
        swarm.particleBestFitness(a) = cftn;
        
        % Update the global best
        if(feval(swarmSettings.maxOrMin, cftn, swarm.bestFitness) == cftn)
            % Update stopping criterion
            swarm.prevBest = swarm.best;
            swarm.stagnation = 0;
            bestUpdated = true;
            
            swarm.best = swarm.pos(a, :);
            swarm.bestFitness = cftn;
        end
    end
end

% Stagnation condition
if(~bestUpdated)
    swarm.stagnation = swarm.stagnation + 1;
end
end

function plotSwarm(swarmSettings, swarm)
% Cache sizes
szs = size(swarm.pos);

clf;
scatter(1 : szs(1), swarm.pos(:, 1)), hold on;
for a = 2 : szs(2)
    scatter(1 : szs(1), swarm.pos(:, a)), hold on;
end
axis([1 szs(1) swarmSettings.imMin swarmSettings.imMax]);
pause(swarmSettings.pauseTime);
end

%swarm.pos = sort(randi([swarmSettings.imMin swarmSettings.imMax], swarmSettings.size, numThresh), 2);
%swarm.pos = sort(fix(rand(swarmSettings.size, numThresh).* ...
%        repmat(255, swarmSettings.size, numThresh)), 2);