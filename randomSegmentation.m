function [Iout, thresholds, bestFitness, time] = randomSegmentation(Iin, numSegments, ...
                  fitnessFunction, maxOrMinFn, numSols, minVal, maxVal)
tic;              
% Create random solutions given the constraints
solutions = sort(randi([minVal maxVal], numSols, numSegments - 1), 2);
% Evaluate
func = @(solutions) fitnessFunction(Iin, solutions);
fitness = cellfun(func, num2cell(solutions, 2));

% Find the best fitness
bestFitness = feval(maxOrMinFn, fitness);

% Assign outputs
thresholds = solutions(find(fitness == bestFitness), :);
Iout = segmentImage(Iin, thresholds);
time = toc;
end
