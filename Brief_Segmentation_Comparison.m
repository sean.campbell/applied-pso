% Typical script start-up
clear variables;
close all;
clc;

% Load a test image
im = im2double(imread('testImageB.jpg'));
numLevels = 3;

% Display the initial image
figure, imshow(im), title('Original Image');

% Perform thresholding based upon the Otsu threshold
ot = multithresh(im, numLevels-1);
showColourSegments(segmentImage(im, ot));
title('Otsu Threshold');

% Perform K-means clustering
[cluster_idx, cluster_center] = kmeans(reshape(im, numel(im), 1), numLevels, 'distance', ...
        'sqEuclidean', 'Replicates', 20, 'Start', 'sample');
imcp = zeros(size(im));
for a = 1 : numLevels
    imcp(cluster_idx == a) = a;
end
figure, showColourSegments(imcp), title('K-Means');    

% Perform seeded region growing
%reg = regionGrowing(im, 78, 115, 0.1);
%figure, imshow(~reg), title('Seeded Region Growing');

% Perform parametric deformable model
% Provide a reasonable starting mask; used a block
figure, imshow(im);
h = imfreehand();
init_msk = h.createMask();
close(gcf);
bw = activecontour(im, init_msk, 300, 'Chan-Vese');
figure, imshow(~bw), title('Level Set');
