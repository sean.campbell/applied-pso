function [Iout] = segmentImage(im, thresholds)
% Validity check
numThresholds = length(thresholds);
assert (numThresholds >= 1);

% Initialisation
Iout = zeros(size(im)); 

% First threshold always lower
Iout(im <= thresholds(1)) = 1;

if(numThresholds == 1)
    Iout(im > thresholds(1)) = 2;
else
    % Set middle thresholds
    for a = 2 : numThresholds
        Iout((im > thresholds(a - 1)) & (im <= thresholds(a))) = a;
    end

    % Set final threshold
    Iout(im > thresholds(numThresholds)) = numThresholds + 1;
end
end
